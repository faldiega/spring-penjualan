<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f"%>
<jsp:include page="template/header.jsp" />
<jsp:include page="template/sidebar.jsp" />
<jsp:include page="template/topbar.jsp" />



<!-- Begin Page Content -->
<div class="container-fluid">

	<!-- Page Heading -->
	<h1 class="h3 mb-4 text-gray-800">Selamat Datang</h1>

	<div class="row">
		<div class="col-lg-10 mx-auto">
			<div id="carouselExampleInterval" class="carousel slide"
				data-ride="carousel">
				<div class="carousel-inner">
					<div class="carousel-item active" data-interval="2000">

						<img src="${pageContext.request.contextPath}/resources/img/1.jpg"
							style="width: 480px; height: 320px;" class="d-block w-100"
							alt="...">
					</div>
					<div class="carousel-item" data-interval="2000">
						<img
							src="${pageContext.request.contextPath}/resources/img/2.jpg"
							style="width: 480px; height: 320px;" class="d-block w-100"
							alt="...">
					</div>
					<div class="carousel-item">
						<img src="${pageContext.request.contextPath}/resources/img/3.jpg"
							style="width: 480px; height: 320px;" class="d-block w-100"
							alt="...">
					</div>
					<div class="carousel-item">
						<img src="${pageContext.request.contextPath}/resources/img/4.jpg"
							style="width: 480px; height: 320px;" class="d-block w-100"
							alt="...">
					</div>
				</div>
				<a class="carousel-control-prev" href="#carouselExampleInterval"
					role="button" data-slide="prev"> <span
					class="carousel-control-prev-icon" aria-hidden="true"></span> <span
					class="sr-only">Previous</span>
				</a> <a class="carousel-control-next" href="#carouselExampleInterval"
					role="button" data-slide="next"> <span
					class="carousel-control-next-icon" aria-hidden="true"></span> <span
					class="sr-only">Next</span>
				</a>
			</div>
		</div>
	</div>

</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->

<jsp:include page="template/footer.jsp" />

