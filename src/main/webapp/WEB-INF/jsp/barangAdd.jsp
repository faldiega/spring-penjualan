<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<jsp:include page="template/header.jsp" />
<jsp:include page="template/sidebar.jsp" />
<jsp:include page="template/topbar.jsp" />

<!-- Begin Page Content -->
<div class="container-fluid">

	<!-- Page Heading -->
	<h1 class="h3 mb-4 text-gray-800">Tambah Barang Baru</h1>

	<form:form action="${pageContext.request.contextPath}/barang/save"
		modelAttribute="dtoAdd">
		<table class="add-barang">
			<tr>
				<td>Kode Barang</td>
				<td>:</td>
				<td><form:input path="kodeBarang" /></td>
				<td><form:errors path="kodeBarang" /></td>
			</tr>
			<tr>
				<td>Nama Barang</td>
				<td>:</td>
				<td><form:input path="namaBarang" /></td>
				<td><form:errors path="namaBarang" /></td>
			</tr>
			<tr>
				<td>Stok</td>
				<td>:</td>
				<td><form:input type="number" path="stokBarang" /></td>
				<td><form:errors path="stokBarang" /></td>
			</tr>
			<tr>
				<td>Kode Supplier</td>
				<td>:</td>
				<td><form:input path="kodeSupplier" /></td>
				<td><form:errors path="kodeSupplier" /></td>
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td colspan="2"><input type="submit" class="btn btn-success btn-sm"
					value="Simpan" />
					<a href="${pageContext.request.contextPath}/barang/index"
					class="btn btn-danger btn-sm">Kembali</a></td>
			</tr>
		</table>

	</form:form>

</div>
<!-- /.container-fluid -->

<jsp:include page="template/footer.jsp" />