<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f"%>
<jsp:include page="template/header.jsp" />
<jsp:include page="template/sidebar.jsp" />
<jsp:include page="template/topbar.jsp" />



<!-- Begin Page Content -->
<div class="container-fluid">

	<!-- Page Heading -->
	<h1 class="h3 mb-4 text-gray-800">Data User</h1>

	<div class="row mt-1 mx-auto">
		<div class="col">
			<div class="card">
				<h5 class="card-header">Detail User</h5>



				<div class="card-body">
					<div class="row">
						<div class="col-md-6">
							<h5 class="card-title">Kode Karyawan</h5>
							<p class="card-text">${users.kodeKaryawan}</p>
							<h5 class="card-title">Nama</h5>
							<p class="card-text">${users.namaKaryawan}</p>
						</div>

						<div class="col-md-6">
							<h5 class="card-title">Username</h5>
							<p class="card-text">${users.username}</p>
							<h5 class="card-title">Password</h5>
							<p class="card-text">${users.password}</p>

							<%-- 	<h5 class="card-title">Jenis Kelamin</h5>
							<c:choose>
								<c:when test="${users.jenisKelamin == 'L'}">
									<p class="card-text">Laki-laki</p>
								</c:when>
								<c:otherwise>
									<p class="card-text">Perempuan</p>
								</c:otherwise>
							</c:choose> --%>


						</div>
					</div>

				</div>
			</div>
		</div>
	</div>

</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->

<jsp:include page="template/footer.jsp" />

