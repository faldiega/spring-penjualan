<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<jsp:include page="template/header.jsp" />
<jsp:include page="template/sidebar.jsp" />
<jsp:include page="template/topbar.jsp" />

<!-- Begin Page Content -->
<div class="container-fluid">

	<!-- Page Heading -->
	<h1 class="h3 mb-4 text-gray-800">Data Karyawan</h1>

	<div class="col">
		<div class="row">
			<!-- Button trigger modal -->
			<div class="col-6">
				<button type="button" class="btn btn-primary btn-sm"
					data-toggle="modal" data-target="#modalTambah">Tambah</button>
			</div>
			<div class="col-4 mx-auto">
				<form class="form-inline"
					action="${pageContext.request.contextPath}/karyawan/pagekaryawan">
					<input class="form-control mr-sm-2" name="cari"
						value="${param.cari}" placeholder="Cari" aria-label="Search">
					<button class="btn btn-outline-success my-2 my-sm-0" type="submit">Cari</button>
				</form>
			</div>
		</div>

		<br>

		<table class="table">
			<tr>
				<th scope="col">Kode Karyawan</th>
				<th scope="col">Nama Karyawan</th>
				<th scope="col">Username</th>
				<th scope="col">Password</th>
				<th scope="col">Aksi</th>
			</tr>
			<c:forEach items="${karyawan}" var="x">
				<tr>
					<td>${x.kodeKaryawan}</td>
					<td>${x.namaKaryawan}</td>
					<td>${x.username}</td>
					<td>${x.password}</td>
					<td>
						<button type="button" class="badge badge-success btnubahkaryawan"
							data-toggle="modal" data-target="#modalUbah">Ubah</button> 
						<a
						href="${pageContext.request.contextPath}/karyawan/delete/${x.kodeKaryawan}"
						onclick="return confirm('Apakah anda yakin?')"
						class="badge badge-danger">Hapus</a>
					</td>
				</tr>
			</c:forEach>
		</table>

		<div class="row justify-content-center">
			<nav aria-label="Page navigation example">
				<ul class="pagination">
					<c:forEach var="i" begin="1" end="${total}">
						<li class="page-item"><a class="page-link"
							href="${pageContext.request.contextPath}/karyawan/pagekaryawan?page=${i}&cari=${param.cari}">${i}</a></li>
					</c:forEach>
				</ul>
			</nav>
		</div>

	</div>
	<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->

<!-- Modal Tambah -->
<div class="modal fade" id="modalTambah" tabindex="-1" role="dialog"
	aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Tambah</h5>
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="text-center">
					<h4 class="h4 text-gray-900 mb-4">${info}</h4>
				</div>
				<form:form action="${pageContext.request.contextPath}/karyawan/save"
					method="POST" modelAttribute="dtoAdd">
					<div class="form-group">
						<label for="kodeKaryawan">Kode Karyawan</label>
						<form:input required="true" path="kodeKaryawan"
							type="kodeKaryawan" class="form-control" id="kodeKaryawan" />
						<%-- 						<small id="error" class="form-text text-danger"><form:errors
								path="kodeKaryawan" /></small> --%>
					</div>
					<div class="form-group">
						<label for="namaKaryawan">Nama Karyawan</label>
						<form:input required="true" path="namaKaryawan"
							type="namaKaryawan" class="form-control" id="namaKaryawan" />
						<%-- 
						<small id="error" class="form-text text-danger"><form:errors
								path="namaKaryawan"/></small> --%>
					</div>
					<div class="form-group">
						<label for="username">Username</label>
						<form:input required="true" path="username" type="username"
							class="form-control" id="username" />
						<small id="error" class="form-text text-danger"><form:errors
								path="username" /></small>
					</div>
					<div class="form-group">
						<label for="password">Password</label>
						<form:input required="true" path="password" type="password"
							class="form-control" id="password" />
						<small id="error" class="form-text text-danger"><form:errors
								path="password" /></small>
					</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
				<input type="submit" class="btn btn-primary" value="Simpan">
				</form:form>
			</div>
		</div>
	</div>
</div>

<!-- Modal Ubah -->

<div class="modal fade" id="modalUbah" tabindex="-1" role="dialog"
	aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Ubah</h5>
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">

				<form:form action="${pageContext.request.contextPath}/karyawan/save"
					method="POST" modelAttribute="dtoAdd">

					<div class="form-group">
						<label for="kodeKaryawan">Kode Karyawan</label>
						<form:input path="kodeKaryawan" type="kodeKaryawan"
							readonly="true" class="form-control" id="kodeKaryawanDat"
							value="${x.kodeKaryawan}" />
					</div>

					<div class="form-group">
						<label for="namaKaryawan">Nama Karyawan</label>
						<form:input required="true" path="namaKaryawan"
							type="namaKaryawan" class="form-control" id="namaKaryawanDat"
							value="${x.namaKaryawan }" />
					</div>
					<div class="form-group">
						<label for="username">Username</label>
						<form:input required="true" path="username" type="username"
							class="form-control" value="${x.username }" id="usernameDat" />
					</div>
					<div class="form-group">
						<label for="password">Password</label>
						<form:input required="true" path="password" type="password"
							class="form-control" id="passwordDat" value="${x.password }" />
					</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
				<button type="submit" class="btn btn-primary">Ubah</button>

				</form:form>
			</div>
		</div>
	</div>
</div>

<!-- Modal Confirm Hapus -->
<div id="modalHapus" class="modal fade">
	<div class="modal-dialog modal-confirm">
		<div class="modal-content">
			<div class="modal-header">
				<div class="icon-box">
					<i class="material-icons">&#xE5CD;</i>
				</div>
				<h4 class="modal-title">Are you sure?</h4>
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true">&times;</button>
			</div>
			<div class="modal-body">
				<p>Do you really want to delete these records? This process
					cannot be undone.</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-info" data-dismiss="modal">Cancel</button>
				<button type="button" class="btn btn-danger">Delete</button>
			</div>
		</div>
	</div>
</div>
<jsp:include page="template/footer.jsp" />
<script>
	$(document).ready(function() {
		var info = '${info}';

		if (info) {
			$('#modalTambah').modal('show');
		}
	})
</script>