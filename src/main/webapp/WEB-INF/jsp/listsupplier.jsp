<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<jsp:include page="template/header.jsp" />
<jsp:include page="template/sidebar.jsp" />
<jsp:include page="template/topbar.jsp" />

<!-- Begin Page Content -->
<div class="container-fluid">

	<!-- Page Heading -->
	<h1 class="h3 mb-4 text-gray-800">Data Supplier</h1>

	<div class="col">
		<div class="row">
			<!-- Button trigger modal -->
			<div class="col-6">
				<button type="button" class="btn btn-primary btn-sm"
					data-toggle="modal" data-target="#modalTambah">Tambah</button>
			</div>
			<div class="col-4 mx-auto">
				<form class="form-inline"
					action="${pageContext.request.contextPath}/supplier/pagesupplier">
					<input class="form-control mr-sm-2" name="cari"
						value="${param.cari}" placeholder="Cari" aria-label="Search">
					<button class="btn btn-outline-success my-2 my-sm-0" type="submit">Cari</button>
				</form>
			</div>
		</div>

		<br>

		<table class="table">
			<tr>
				<th scope="col">Kode Supplier</th>
				<th scope="col">Nama Supplier</th>
				<th scope="col">Alamat Supplier</th>
				<th scope="col">Telepon Supplier</th>
				<th scope="col">Email Supplier</th>
				<th scope="col">Kode Kota</th>
			</tr>
			<c:forEach items="${supplier}" var="x">
				<tr>
					<td>${x.kodeSupplier}</td>
					<td>${x.namaSupplier}</td>
					<td>${x.alamatSupplier}</td>
					<td>${x.telpSupplier}</td>
					<td>${x.emailSupplier}</td>
					<td>${x.kodeKota}</td>
					<td>
						<button type="button" class="badge badge-success btnubahsupplier"
							data-toggle="modal" data-target="#modalUbah">Ubah</button> <a
						href="${pageContext.request.contextPath}/supplier/delete/${x.kodeSupplier}"
						onclick="return confirm('Apakah anda yakin?')"
						class="badge badge-danger">Hapus</a>
					</td>
				</tr>
			</c:forEach>
		</table>

		<div class="row justify-content-center">
			<nav aria-label="Page navigation example">
				<ul class="pagination">
					<c:forEach var="i" begin="1" end="${total}">
						<li class="page-item"><a class="page-link"
							href="${pageContext.request.contextPath}/supplier/pagesupplier?page=${i}&cari=${param.cari}">${i}</a></li>
					</c:forEach>
				</ul>
			</nav>
		</div>

	</div>
	<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->

<jsp:include page="template/footer.jsp" />
<script>
	$(document).ready(function() {
		var info = '${info}';

		if (info) {
			$('#modalTambah').modal('show');
		}
	})
</script>