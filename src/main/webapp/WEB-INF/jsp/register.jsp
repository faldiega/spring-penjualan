<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">

<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">

<title>SB Admin 2 - Register</title>

<!-- Custom fonts for this template-->
<link
	href="${pageContext.request.contextPath}/resources/vendor/sbadmin2/vendor/fontawesome-free/css/all.min.css"
	rel="stylesheet" type="text/css">
<link
	href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
	rel="stylesheet">

<!-- Custom styles for this template-->
<link
	href="${pageContext.request.contextPath}/resources/css/sb-admin-2.min.css"
	rel="stylesheet">

</head>

<body class="bg-gradient-primary">

	<div class="container">

		<div class="card o-hidden border-0 shadow-lg my-5 col-lg-7 mx-auto">
			<div class="card-body p-0">
				<!-- Nested Row within Card Body -->
				<div class="row">
					<div class="col-lg">
						<div class="p-5">
							<div class="text-center">
								<h1 class="h4 text-gray-900 mb-4">Create an Account!</h1>
							</div>
							<div class="text-center">
								<h4 class="h4 text-gray-900 mb-4">${info}</h4>
							</div>
							<form:form action="${pageContext.request.contextPath}/auth/save"
								method="POST" modelAttribute="dtotambah">

								<div class="form-group">
									<form:input required="true" class="form-control"
										path="kodeKaryawan" placeholder="Masukan Kode Karyawan" />
								</div>

								<div class="form-group">
									<form:input required="true" class="form-control"
										path="namaKaryawan" placeholder="Masukan Nama" />
								</div>
								<div class="form-group">
									<form:input required="true" class="form-control"
										path="username" placeholder="Masukan username" />
								</div>
								<div class="form-group">
									<form:input type="password" required="true" class="form-control"
										path="password" placeholder="Masukan password" />
								</div>
								<button type="submit" class="btn btn-primary btn-user btn-block">
									Register Account</button>
								<hr>
							</form:form>
							<hr>
							<div class="text-center">
								<a class="small"
									href="${pageContext.request.contextPath}/auth/index">Already
									have an account? Login!</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>

	<!-- Bootstrap core JavaScript-->
	<script
		src="${pageContext.request.contextPath}/resources/vendor/sbadmin2/vendor/jquery/jquery.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/vendor/sbadmin2/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

	<!-- Core plugin JavaScript-->
	<script
		src="${pageContext.request.contextPath}/resources/vendor/sbadmin2/vendor/jquery-easing/jquery.easing.min.js"></script>

	<!-- Custom scripts for all pages-->
	<script
		src="${pageContext.request.contextPath}/resources/js/sb-admin-2.min.js"></script>

</body>

</html>
