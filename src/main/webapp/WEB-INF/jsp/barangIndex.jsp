<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f"%>
<jsp:include page="template/header.jsp" />
<jsp:include page="template/sidebar.jsp" />
<jsp:include page="template/topbar.jsp" />

<!-- Begin Page Content -->
<div class="container-fluid">

	<!-- Page Heading -->
	<h1 class="h3 mb-4 text-gray-800">Data Barang</h1>

	<input type="button" class="btn btn-success" value="Tambah Barang"
		onclick="location.href='${pageContext.request.contextPath}/barang/add'" />

	<div class="search-box">
		<label for="">Search</label> 
		<input type="text">
	</div>

	<table class="tabel-barang">
		<tr>
			<th>Kode Barang</th>
			<th>Nama Barang</th>
			<th>Stok</th>
			<th>Kode Supplier</th>
			<th>Action</th>
		</tr>
		<c:forEach items="${indexBarang}" var="bar">
			<tr>
				<td>${bar.kodeBarang}</td>
				<td>${bar.namaBarang}</td>
				<td>${bar.stokBarang}</td>
				<td>${bar.kodeSupplier}</td>
				<td><a
					href="${pageContext.request.contextPath}/barang/edit/${bar.kodeBarang}"
					class="btn btn-warning btn-sm">Edit</a> <a
					href="${pageContext.request.contextPath}/barang/delete/${bar.kodeBarang}"
					class="btn btn-danger btn-sm">Delete</a></td>
			</tr>
		</c:forEach>
	</table>

</div>
<!-- /.container-fluid -->

<jsp:include page="template/footer.jsp" />