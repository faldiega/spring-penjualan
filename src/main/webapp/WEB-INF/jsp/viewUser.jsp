<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %> 
<jsp:include page="template/header.jsp" /> 
<jsp:include page="template/sidebar.jsp" /> 
<jsp:include page="template/topbar.jsp" /> 

    

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-4 text-gray-800">View User</h1>
          
          <div class="col">
          <div class="row">
          <!-- Button trigger modal -->
          	<div class="col-6">
          		<button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modalTambah">
					Tambah User
				</button>
          	</div>
          	
          	<div class="col-4">
          	<form:form action="${pageContext.request.contextPath}/user/pageuser" method="GET">
          		<input class="form-control" type="text" name="cari" value="${param.cari}">
          	</div>
          	<div class="col-2">
          		<input class="btn btn-primary btn-sm" type="submit" value="cari">
          	</form:form>
          	</div>
          	</div>
          </div>
          
			<br>
          
          <table class="table">
			  <tr >
			    <th scope="col">User ID</th>
			    <th scope="col">Username</th>
			    <th scope="col">Nama</th>
			    <th scope="col">Jenis Kelamin</th>
			    <th scope="col">Tanggal Lahir</th>
			    <th scope="col">Alamat</th>
			    <th scope="col">No Telepon</th>
			    <th scope="col">Password</th>
			    <th scope="col">Action</th>
			  </tr>
			  <c:forEach items="${DaftarList}" var="prnt">
			  <tr>
			    <td>${prnt.userId}</td>
			    <td>${prnt.username}</td>
			    <td>${prnt.nama}</td>
			    <td>${prnt.jenisKelamin}</td>
			    
			    <td><f:formatDate pattern="dd/MM/yyyy" value="${prnt.tanggalLahir}"/></td>
			    <td>${prnt.alamat}</td>
			    <td>${prnt.noTelpon}</td>
			    <td>${prnt.password}</td>
			    <td>
			    	<button type="button" class="badge badge-success btnubahuser">Ubah</button>
	    			<a href="${pageContext.request.contextPath}/user/delete/${prnt.userId}" class="badge badge-danger" onclick="return confirm('Apakah kamu yakin?')">Hapus</a>
	    		</td>
			  </tr>
			  </c:forEach>
			</table>
			<nav aria-label="Page navigation example">
			  <ul class="pagination justify-content-center">
			    <li class="page-item">
			      <a class="page-link" href="${pageContext.request.contextPath}/user/pageuser?page=1&cari=${param.cari}">First</a>
			    </li>
			    <c:forEach var = "i" begin = "1" end = "${jumlahHalaman}">
			    <li class="page-item"><a class="page-link" href="${pageContext.request.contextPath}/user/pageuser?page=${i}&cari=${param.cari}">${i}</a></li>
			    </c:forEach>
			    <li class="page-item">
			      <a class="page-link" href="${pageContext.request.contextPath}/user/pageuser?page=${jumlahHalaman}&cari=${param.cari}">Last</a>
			    </li>
			  </ul>
			</nav>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->
      
      
      
      <!-- Modal Tambah -->
		<div class="modal fade" id="modalTambah" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <h5 class="modal-title" id="exampleModalLabel">Tambah</h5>
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
		      </div>
		      <div class="modal-body">
		        <form:form action="${pageContext.request.contextPath}/user/save" method="POST" modelAttribute="dtotambah"> <!-- ada disini -->
		        	  <div class="form-group">
					    <label for="formGroupExampleInput">Masukan User Name : </label>
					    <form:input class="form-control" path="username" />
					  </div>
					  <div class="form-group">
					    <label for="formGroupExampleInput2">Masukan Nama : </label>
					    <form:input class="form-control" path="nama" />
					  </div>
					  <div class="form-group">
					    <label for="formGroupExampleInput2">Masukan Jenis Kelamin : </label>
					    <form:select class="form-control" path="jenisKelamin">
					    	<form:option class="form-control"  value="P">Perempuan</form:option>
					    	<form:option value="L">Laki-laki</form:option>
					    </form:select>
					  </div>
					  <div class="form-group">
					    <label for="formGroupExampleInput2">Masukan Tanggal Lahir : </label>
					    <form:input class="form-control" path="tanggalLahir" />
					  </div>
					  <div class="form-group">
					    <label for="formGroupExampleInput2">Masukan Alamat : </label>
					    <form:input class="form-control" path="alamat" />
					  </div>
					  <div class="form-group">
					    <label for="formGroupExampleInput2">Masukan No Telepon : </label>
					    <form:input class="form-control" path="noTelpon" />
					    
					  </div>
					  <div class="form-group">
					    <label for="formGroupExampleInput2">Masukan Password : </label>
					    <form:input class="form-control" path="password" />
					  </div>
							        
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
		        <input type="submit" class="btn btn-primary" value="Simpan">
		        </form:form>
		      </div>
		    </div>
		  </div>
		</div>
		
		<!-- Modal Ubah -->
		<div class="modal fade" id="modalUbah" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <h5 class="modal-title" id="exampleModalLabel">Ubah</h5>
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
		      </div>
		      <div class="modal-body">
		        <form:form action="${pageContext.request.contextPath}/user/edit" method="POST" modelAttribute="dtoubah"> <!-- ada disini -->
		        	  
		        	  <div class="form-group">
					    <label for="formGroupExampleInput">Masukan User Id : </label>
					    <form:input class="form-control" id="userIdD" path="userId" readonly="true"/>
					  </div>
		        	  <div class="form-group">
					    <label for="formGroupExampleInput">Masukan User Name : </label>
					    <form:input class="form-control" id="usernameD" path="username" />
					  </div>
					  <div class="form-group">
					    <label for="formGroupExampleInput2">Masukan Nama : </label>
					    <form:input class="form-control" id="namaD" path="nama" />
					  </div>
					  <div class="form-group">
					    <label for="formGroupExampleInput2">Masukan Jenis Kelamin : </label>
					    <form:select class="form-control" id="jenisKelaminD" path="jenisKelamin">
					    	<form:option class="form-control"  value="P">Perempuan</form:option>
					    	<form:option value="L">Laki-laki</form:option>
					    </form:select>
					  </div>
					  <div class="form-group">
					    <label for="formGroupExampleInput2">Masukan Tanggal Lahir : </label>
					    <form:input class="form-control" id="tanggalLahirD" path="tanggalLahir" />
					  </div>
					  <div class="form-group">
					    <label for="formGroupExampleInput2">Masukan Alamat : </label>
					    <form:input class="form-control" id="alamatD" path="alamat" />
					  </div>
					  <div class="form-group">
					    <label for="formGroupExampleInput2">Masukan No Telepon : </label>
					    <form:input class="form-control" id="noTelponD" path="noTelpon" />
					  </div>
					  <div class="form-group">
					    <label for="formGroupExampleInput2">Masukan Password : </label>
					    <form:input class="form-control" id="passwordD" path="password" />
					  </div>
		        
		      </div>
		      
		      
		      <div class="modal-footer">
		        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
		        <button type="submit" class="btn btn-primary">Ubah</button>
		        </form:form>
		      </div>
		    </div>
		  </div>
		</div>
      
<jsp:include page="template/footer.jsp" /> 