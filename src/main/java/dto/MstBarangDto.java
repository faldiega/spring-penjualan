package dto;

import org.hibernate.validator.constraints.NotEmpty;

public class MstBarangDto {
	@NotEmpty (message = "Kode Barang tidak boleh kosong !")
	private String kodeBarang;
	@NotEmpty (message = "Nama Barang tidak boleh kosong !")
	private String namaBarang;
	/* @NotEmpty (message = "Stok Barang tidak boleh kosong !") */
	private int stokBarang;
	@NotEmpty (message = "Kode Supplier tidak boleh kosong !")
	private String kodeSupplier;

	public String getKodeBarang() {
		return kodeBarang;
	}

	public void setKodeBarang(String kodeBarang) {
		this.kodeBarang = kodeBarang;
	}

	public String getNamaBarang() {
		return namaBarang;
	}

	public void setNamaBarang(String namaBarang) {
		this.namaBarang = namaBarang;
	}

	public int getStokBarang() {
		return stokBarang;
	}

	public void setStokBarang(int stokBarang) {
		this.stokBarang = stokBarang;
	}

	public String getKodeSupplier() {
		return kodeSupplier;
	}

	public void setKodeSupplier(String kodeSupplier) {
		this.kodeSupplier = kodeSupplier;
	}
	
}
