package dto;

import org.hibernate.validator.constraints.NotEmpty;

public class MstKaryawanDto {
	@NotEmpty (message = "Kode Karyawan tidak boleh kosong !")
	private String kodeKaryawan;
	@NotEmpty (message = "Nama tidak boleh kosong !")
	private String namaKaryawan;
	@NotEmpty (message = "Password tidak boleh kosong !")
	private String password;
	@NotEmpty (message = "Username tidak boleh kosong !")
	private String username;
	/**
	 * @return the kodeKaryawan
	 */
	public String getKodeKaryawan() {
		return kodeKaryawan;
	}
	/**
	 * @param kodeKaryawan the kodeKaryawan to set
	 */
	public void setKodeKaryawan(String kodeKaryawan) {
		this.kodeKaryawan = kodeKaryawan;
	}
	/**
	 * @return the namaKaryawan
	 */
	public String getNamaKaryawan() {
		return namaKaryawan;
	}
	/**
	 * @param namaKaryawan the namaKaryawan to set
	 */
	public void setNamaKaryawan(String namaKaryawan) {
		this.namaKaryawan = namaKaryawan;
	}
	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}
	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}
	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}
	
	

}
