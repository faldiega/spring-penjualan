package dto;

import org.hibernate.validator.constraints.NotEmpty;

public class MstSupplierDto {
	@NotEmpty (message = "Kode Supplier tidak boleh kosong !")
	private String kodeSupplier;
	@NotEmpty (message = "Alamat Supplier tidak boleh kosong !")
	private String alamatSupplier;
	@NotEmpty (message = "Email Supplier tidak boleh kosong !")
	private String emailSupplier;
	@NotEmpty (message = "Kode Kota tidak boleh kosong !")
	private String kodeKota;
	@NotEmpty (message = "Nama Supplier tidak boleh kosong !")
	private String namaSupplier;
	@NotEmpty (message = "No Telepon Supplier tidak boleh kosong !")
	private String telpSupplier;
	
	public String getKodeSupplier() {
		return kodeSupplier;
	}
	public void setKodeSupplier(String kodeSupplier) {
		this.kodeSupplier = kodeSupplier;
	}
	public String getAlamatSupplier() {
		return alamatSupplier;
	}
	public void setAlamatSupplier(String alamatSupplier) {
		this.alamatSupplier = alamatSupplier;
	}
	public String getEmailSupplier() {
		return emailSupplier;
	}
	public void setEmailSupplier(String emailSupplier) {
		this.emailSupplier = emailSupplier;
	}
	public String getKodeKota() {
		return kodeKota;
	}
	public void setKodeKota(String kodeKota) {
		this.kodeKota = kodeKota;
	}
	public String getNamaSupplier() {
		return namaSupplier;
	}
	public void setNamaSupplier(String namaSupplier) {
		this.namaSupplier = namaSupplier;
	}
	public String getTelpSupplier() {
		return telpSupplier;
	}
	public void setTelpSupplier(String telpSupplier) {
		this.telpSupplier = telpSupplier;
	}
}
