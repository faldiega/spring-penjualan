package controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import dto.MstKaryawanDto;

import service.MstKaryawanSvc;

@Controller
@RequestMapping("/auth")
public class AuthCtl {
	@Autowired
	private MstKaryawanSvc svcKaryawan;

	@RequestMapping("/index")
	public String index(@ModelAttribute("login") MstKaryawanDto dto) {
		return "login";
	}

	@RequestMapping("/dashboard")
	public String login(RedirectAttributes redirectAttributes,
			HttpServletRequest request,
			@ModelAttribute("login") MstKaryawanDto dto, Model model) {
		MstKaryawanDto findOneCust = svcKaryawan.findUser(dto.getUsername(),
				dto.getPassword());

		if (findOneCust != null) {

			HttpSession session = request.getSession();
			String userpass = dto.getUsername();
			session.setMaxInactiveInterval(300);
			session.setAttribute("varUser", userpass);

			return "redirect:/profil/dashboard";
		} else {
			redirectAttributes.addFlashAttribute("infox",
					"Username atau Password salah");
			return "redirect:/auth/index ";
		}

	}

	@RequestMapping("/register")
	public String register(MstKaryawanDto dtotambah, Model model) {
		MstKaryawanDto dto = new MstKaryawanDto();
		model.addAttribute("dtotambah", dto);
		return "register";

	}

	@RequestMapping("/save")
	public String saveUser(
			@Valid @ModelAttribute("dtotambah") MstKaryawanDto dto,
			BindingResult result, RedirectAttributes redirectAttributes) {

		if (result.hasErrors()) {
			return "index";
		} else {

			if (svcKaryawan.findUsername(dto.getUsername()) != null) {

				redirectAttributes.addFlashAttribute("info",
						"Maaf username dengan nama " + dto.getUsername()
								+ " sudah terdaftar");
				return "redirect:/auth/register";
			} else {

				svcKaryawan.save(dto);

				return "redirect:/auth/index";
			}
		}

	}

	@RequestMapping("/logout")
	public String logout(HttpServletRequest request,
			HttpServletResponse response) {
		HttpSession session = request.getSession();
		// session.removeValue("varUser");
		session.removeAttribute("varUser");
		// session.setAttribute("varUser", null);
		session.invalidate();
		return "redirect:/auth/index";
	}
}
