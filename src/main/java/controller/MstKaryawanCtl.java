package controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import dto.MstKaryawanDto;
import service.MstKaryawanSvc;

@Controller
@RequestMapping("/karyawan")
public class MstKaryawanCtl {
	@Autowired
	private MstKaryawanSvc svc;
	
	@RequestMapping("/pagekaryawan")
	public String karyawan(@RequestParam(value = "page", defaultValue = "1", required = false)Integer page, @RequestParam(value = "cari", defaultValue = "", required = false)String cari, HttpServletRequest request, Model model)
	{
		HttpSession session = request.getSession();
		String pages = null;
		if(session.getAttribute("varUser") != null){
			Map<String, Object> map = svc.pencarian(cari, page);
		    model.addAttribute("karyawan", map.get("list"));
		    model.addAttribute("total", map.get("jumlah"));
		    MstKaryawanDto dto = new MstKaryawanDto();
			model.addAttribute("dtoAdd", dto);
		    pages = "semuadaftarkaryawan";
		}
		else if(session.getAttribute("varUser") == null)
		{
            pages = "redirect:/auth/index";
        }
		return pages;
	}
	
	@RequestMapping("/save")
	public String save(@Valid @ModelAttribute("dtoAdd")MstKaryawanDto dto, BindingResult result) {
		svc.save(dto);
		return "redirect:/karyawan/pagekaryawan";
	}
	
	@RequestMapping("/delete/{kodeKaryawan}")
	public String deleteData(@PathVariable("kodeKaryawan")String kodeKaryawan) {
		svc.delete(kodeKaryawan);
		return "redirect:/karyawan/pagekaryawan";
	}
}
