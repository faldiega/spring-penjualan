package controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import dto.MstKaryawanDto;
import service.MstKaryawanSvc;


@Controller
@RequestMapping("/profil")
public class MstProfilCtl {
	
	@Autowired
	private MstKaryawanSvc svc;
	
	@RequestMapping("/dashboard")
	public String dashboard(HttpServletRequest request, Model model)
	{
		HttpSession session = request.getSession();
		String pages = null;
		if(session.getAttribute("varUser") != null){
		    String user = (String) session.getAttribute("varUser");
		    //MstUserDto dto = svcUser.findUserByUsername(user);
		    MstKaryawanDto dto = svc.findUsername(user); 
		    model.addAttribute("users", dto);
		    model.addAttribute("user", dto.getUsername());
		pages = "dashboard";
		}
		else if(session.getAttribute("varUser") == null)
		{
            pages = "redirect:/auth/index";
        }
		return pages;
	}

	@RequestMapping("/pageuser")
	public String datauser(HttpServletRequest request, Model model)
	{
		HttpSession session = request.getSession();
		String pages = null;
		if(session.getAttribute("varUser") != null){
		    String user = (String) session.getAttribute("varUser");
		    //MstUserDto dto = svcUser.findUserByUsername(user);
		    MstKaryawanDto dto = svc.findUsername(user); 
		    model.addAttribute("users", dto);
		    model.addAttribute("user", dto.getUsername());
		pages = "datauser";
		}
		else if(session.getAttribute("varUser") == null)
		{
            pages = "redirect:/auth/index";
        }
		return pages;
	}

}
