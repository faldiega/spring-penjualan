package controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import dto.MstBarangDto;
import dto.MstKaryawanDto;
import service.MstBarangSvc;

@Controller
@RequestMapping("/barang")
public class MstBarangCtl {

	@Autowired
	private MstBarangSvc svc;
	
	@RequestMapping("/index")
	public String index(Model mod) {
		List<MstBarangDto> dtos = svc.allBarang();
		mod.addAttribute("indexBarang", dtos);
		return "barangIndex";
	}
	
	@RequestMapping("/add")
	public String add(Model mod) {
		MstBarangDto dto = new MstBarangDto();
		mod.addAttribute("dtoAdd", dto);
		return "barangAdd";
	}
	
	@RequestMapping("/save")
	public String save(@Valid @ModelAttribute("dtoAdd") MstBarangDto dto, BindingResult result) {
		if (result.hasErrors()) {
			return "barangAdd";
		} else {
			svc.save(dto);
			return "redirect:/barang/index";
		}
	}
	
	@RequestMapping("/edit/{kodeBarang}")
	public String edit(@PathVariable("kodeBarang") String kodeBarang, Model mod) {
		MstBarangDto dto = svc.findOne(kodeBarang);
		mod.addAttribute("dtoUpdate", dto);
		return "barangEdit";
	}
	
	@RequestMapping("/editsave")
	public String editSave(@ModelAttribute("dtoUpdate") MstBarangDto dto, BindingResult result) {
		if (result.hasErrors()) {
			return "barangEdit";
		} else {
			svc.update(dto);
			return "redirect:/barang/index";
		}
	}
	
	@RequestMapping("/delete/{kodeBarang}")
	public String deleteData(@PathVariable("kodeBarang") String kodeBarang) {
		svc.delete(kodeBarang);
		return "redirect:/barang/index";
	}
	
	
}
