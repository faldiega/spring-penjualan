package controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import dto.MstSupplierDto;
import service.MstSupplierSvc;

@Controller
@RequestMapping("/supplier")
public class MstSupplierCtl {
	@Autowired
	private MstSupplierSvc svc;
	
	@RequestMapping("/pagesupplier")
	public String dashboard(@RequestParam(value = "page", defaultValue = "1", required = false)Integer page, @RequestParam(value = "cari", defaultValue = "", required = false)String cari, HttpServletRequest request, Model model)
	{
		HttpSession session = request.getSession();
		String pages = null;
		if(session.getAttribute("varUser") != null){
			Map<String, Object> map = svc.pencarian(cari, page); 
		    model.addAttribute("supplier", map.get("list"));
		    model.addAttribute("total", map.get("jumlah"));
		    pages = "listsupplier";
		}
		else if(session.getAttribute("varUser") == null)
		{
            pages = "redirect:/auth/index";
        }
		return pages;
	}
}
