package dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import entity.MstBarang;
import entity.MstBarangPK;

public interface MstBarangDao extends JpaRepository<MstBarang, MstBarangPK> {
	@Query("SELECT bar FROM MstBarang bar")
	public List<MstBarang> findAllBarang();
	
	@Query("SELECT bar, sup.namaSupplier FROM MstBarang bar, MstSupplier sup "
			+ "WHERE bar.kodeBarang = :cari OR bar.namaBarang LIKE %:cari%")
	public List<MstBarang> findAllBarangBySearch();
}
