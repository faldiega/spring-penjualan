package dao;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import entity.MstKaryawan;
import entity.MstKaryawanPK;

public interface MstKaryawanDao extends
		JpaRepository<MstKaryawan, MstKaryawanPK> {

	@Query("SELECT a FROM MstKaryawan a WHERE a.namaKaryawan LIKE %:cari%")
	public List<MstKaryawan> pencarian(@Param("cari") String cari,
			Pageable pageable);

	@Query("SELECT COUNT(a.kodeKaryawan) FROM MstKaryawan a WHERE a.namaKaryawan LIKE %:cari%")
	public int jumlahData(@Param("cari") String cari);

	@Query("select a from MstKaryawan a where a.username = :username and a.password = :password")
	public MstKaryawan findCustByusernamePassword(
			@Param("username") String username,
			@Param("password") String password);

	@Query("select a from MstKaryawan a where a.username = :username")
	public MstKaryawan findCustByusername(@Param("username") String username);

}
