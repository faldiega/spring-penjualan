package dao;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import entity.*;

public interface MstSupplierDao extends
JpaRepository<MstSupplier, MstSupplierPk>{
	
	@Query("SELECT a FROM MstSupplier a WHERE a.namaSupplier LIKE %:cari%")
	public List<MstSupplier> pencarian(@Param("cari") String cari,
			Pageable pageable);
	
	@Query("SELECT COUNT(a.kodeSupplier) FROM MstSupplier a WHERE a.namaSupplier LIKE %:cari%")
	public int jumlahData(@Param("cari") String cari);
}
