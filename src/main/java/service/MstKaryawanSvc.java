package service;

import java.util.List;
import java.util.Map;

import dto.MstKaryawanDto;

public interface MstKaryawanSvc {

	public List<MstKaryawanDto> semuaKaryawan();

	public MstKaryawanDto ambilSatu(String kode);

	public void save(MstKaryawanDto dto);

	public void update(MstKaryawanDto dto);

	public void delete(String kode);

	public Map<String, Object> pencarian(String cari, int page);

	public MstKaryawanDto findUser(String username, String password);

	public MstKaryawanDto findUsername(String username);

}
