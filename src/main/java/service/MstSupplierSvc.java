package service;

import java.util.List;
import java.util.Map;

import dto.*;

public interface MstSupplierSvc {
	public List<MstSupplierDto> allSupplier();
	public MstSupplierDto findOne(String kodeSupplier);
	public void save(MstSupplierDto dto);
	public void update(MstSupplierDto dto);
	public void delete(String kodeSupplier);
	public Map<String, Object> pencarian(String cari, int page);
}
