package service;

import java.util.List;

import dto.MstBarangDto;

public interface MstBarangSvc {
	public List<MstBarangDto> allBarang();
	
	public MstBarangDto findOne(String kodeBarang);
	
	public void save(MstBarangDto dto);
	
	public void update(MstBarangDto dto);
	
	public void delete(String kodeBarang);
	
}
