package service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import dao.MstBarangDao;
import dto.MstBarangDto;
import entity.MstBarang;
import entity.MstBarangPK;
import service.MstBarangSvc;

@Service
@Transactional
public class MstBarangSvcImpl implements MstBarangSvc {

	@Autowired
	MstBarangDao dao;

	@Override
	public List<MstBarangDto> allBarang() {
		List<MstBarang> listBarang = dao.findAllBarang();
		List<MstBarangDto> listDto = new ArrayList<>();
		for (MstBarang bar : listBarang) {
			MstBarangDto newDto = new MstBarangDto();
			newDto.setKodeBarang(bar.getKodeBarang());
			newDto.setNamaBarang(bar.getNamaBarang());
			newDto.setStokBarang(bar.getStokBarang());
			newDto.setKodeSupplier(bar.getKodeSupplier());
			listDto.add(newDto);
		}
		return listDto;
	}

	@Override
	public MstBarangDto findOne(String kodeBarang) {
		MstBarangPK pk = new MstBarangPK();
		pk.setKodeBarang(kodeBarang);
		MstBarang bar = dao.findOne(pk);
		if (bar != null) {
			MstBarangDto newDto = new MstBarangDto();
			newDto.setKodeBarang(bar.getKodeBarang());
			newDto.setNamaBarang(bar.getNamaBarang());
			newDto.setStokBarang(bar.getStokBarang());
			newDto.setKodeSupplier(bar.getKodeSupplier());
			return newDto;
		} else {
			return null;
		}
	}

	@Override
	public void save(MstBarangDto dto) {
		MstBarang bar = new MstBarang();
		bar.setKodeBarang(dto.getKodeBarang());
		bar.setNamaBarang(dto.getNamaBarang());
		bar.setStokBarang(dto.getStokBarang());
		bar.setKodeSupplier(dto.getKodeSupplier());
		dao.save(bar);
	}

	@Override
	public void update(MstBarangDto dto) {
		MstBarangPK pk = new MstBarangPK();
		pk.setKodeBarang(dto.getKodeBarang());
		MstBarang bar = dao.findOne(pk);
		if (bar != null) {
			bar.setKodeBarang(dto.getKodeBarang());
			bar.setNamaBarang(dto.getNamaBarang());
			bar.setStokBarang(dto.getStokBarang());
			bar.setKodeSupplier(dto.getKodeSupplier());
			dao.save(bar);
		}
	}

	@Override
	public void delete(String kodeBarang) {
		MstBarangPK pk = new MstBarangPK();
		pk.setKodeBarang(kodeBarang);
		dao.delete(pk);
	}

}
