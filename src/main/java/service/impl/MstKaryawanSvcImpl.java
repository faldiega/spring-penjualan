package service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import dao.MstKaryawanDao;
import dto.MstKaryawanDto;
import entity.MstKaryawan;
import entity.MstKaryawanPK;
import service.MstKaryawanSvc;

@Service
@Transactional
public class MstKaryawanSvcImpl implements MstKaryawanSvc {

	@Autowired
	MstKaryawanDao daoMstKaryawan;

	@Override
	public List<MstKaryawanDto> semuaKaryawan() {
		List<MstKaryawan> listKaryawan = daoMstKaryawan.findAll();
		List<MstKaryawanDto> newListDto = new ArrayList<>();
		for (MstKaryawan kar : listKaryawan) {
			MstKaryawanDto newDtoKaryawan = new MstKaryawanDto();
			newDtoKaryawan.setKodeKaryawan(kar.getKodeKaryawan());
			newDtoKaryawan.setNamaKaryawan(kar.getNamaKaryawan());
			newDtoKaryawan.setUsername(kar.getUsername());
			newDtoKaryawan.setPassword(kar.getPassword());
			newListDto.add(newDtoKaryawan);
		}
		return newListDto;
	}

	@Override
	public MstKaryawanDto ambilSatu(String kode) {
		MstKaryawanPK pk = new MstKaryawanPK();
		pk.setKodeKaryawan(kode);
		MstKaryawan kar = daoMstKaryawan.findOne(pk);
		if (kar != null) {
			MstKaryawanDto newDtoKaryawan = new MstKaryawanDto();
			newDtoKaryawan.setKodeKaryawan(kar.getKodeKaryawan());
			newDtoKaryawan.setNamaKaryawan(kar.getNamaKaryawan());
			newDtoKaryawan.setUsername(kar.getUsername());
			newDtoKaryawan.setPassword(kar.getPassword());
			return newDtoKaryawan;
		} else {
			return null;
		}
	}

	@Override
	public void save(MstKaryawanDto dto) {
		MstKaryawan kar = new MstKaryawan();
		kar.setKodeKaryawan(dto.getKodeKaryawan());
		kar.setNamaKaryawan(dto.getNamaKaryawan());
		kar.setUsername(dto.getUsername());
		kar.setPassword(dto.getPassword());
		daoMstKaryawan.save(kar);

	}

	@Override
	public void update(MstKaryawanDto dto) {

	}

	@Override
	public void delete(String kode) {
		MstKaryawanPK pk = new MstKaryawanPK();
		pk.setKodeKaryawan(kode);
		daoMstKaryawan.delete(pk);

	}

	@Override
	public Map<String, Object> pencarian(String cari, int page) {
		Map<String, Object> map = new HashMap<>();
		int perPage = 5;
		Pageable paging = new PageRequest(page - 1, perPage, new Sort(
				new Sort.Order(Direction.fromString("desc"), "namaKaryawan")));
		List<MstKaryawan> listKaryawan = daoMstKaryawan.pencarian(cari, paging);
		List<MstKaryawanDto> listDtoKaryawan = new ArrayList<>();
		for (MstKaryawan kar : listKaryawan) {
			MstKaryawanDto dtoKaryawan = new MstKaryawanDto();
			dtoKaryawan.setKodeKaryawan(kar.getKodeKaryawan());
			dtoKaryawan.setNamaKaryawan(kar.getNamaKaryawan());
			dtoKaryawan.setUsername(kar.getUsername());
			dtoKaryawan.setPassword(kar.getPassword());
			listDtoKaryawan.add(dtoKaryawan);
		}

		int jumlahData = daoMstKaryawan.jumlahData(cari);
		int jumlahHalaman = 0;
		jumlahHalaman = jumlahData / perPage;
		if (jumlahData % perPage > 0) {
			jumlahHalaman++;
		}
		map.put("list", listDtoKaryawan);
		map.put("jumlah", jumlahHalaman);
		return map;
	}

	@Override
	public MstKaryawanDto findUser(String username, String password) {
		// TODO Auto-generated method stub
		MstKaryawan kar = daoMstKaryawan.findCustByusernamePassword(username,
				password);
		if (kar != null) {
			MstKaryawanDto newDtoKaryawan = new MstKaryawanDto();
			newDtoKaryawan.setKodeKaryawan(kar.getKodeKaryawan());
			newDtoKaryawan.setNamaKaryawan(kar.getNamaKaryawan());
			newDtoKaryawan.setUsername(kar.getUsername());
			newDtoKaryawan.setPassword(kar.getPassword());
			return newDtoKaryawan;
		}
		return null;
	}

	@Override
	public MstKaryawanDto findUsername(String username) {
		// TODO Auto-generated method stub
		
		MstKaryawan kar = daoMstKaryawan.findCustByusername(username);
		if (kar != null) {
			MstKaryawanDto newDtoKaryawan = new MstKaryawanDto();
			newDtoKaryawan.setKodeKaryawan(kar.getKodeKaryawan());
			newDtoKaryawan.setNamaKaryawan(kar.getNamaKaryawan());
			newDtoKaryawan.setUsername(kar.getUsername());
			newDtoKaryawan.setPassword(kar.getPassword());
			return newDtoKaryawan;
		}
		return null;
	}
}
