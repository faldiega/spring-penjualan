package service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import dao.MstSupplierDao;
import dto.MstKaryawanDto;
import dto.MstSupplierDto;
import entity.MstKaryawan;
import entity.MstSupplier;
import entity.MstSupplierPk;
import service.MstSupplierSvc;

@Service
@Transactional
public class MstSupplierSvcImpl implements MstSupplierSvc {
	@Autowired
	MstSupplierDao dao;

	@Override
	public List<MstSupplierDto> allSupplier() {
		// TODO Auto-generated method stub
		List<MstSupplier> list = dao.findAll();
		List<MstSupplierDto> dtos = new ArrayList<>();
		for (MstSupplier item : list) {
			MstSupplierDto dto = new MstSupplierDto();
			dto.setAlamatSupplier(item.getAlamatSupplier());
			dto.setEmailSupplier(item.getEmailSupplier());
			dto.setKodeKota(item.getKodeKota());
			dto.setKodeSupplier(item.getKodeSupplier());
			dto.setNamaSupplier(item.getNamaSupplier());
			dto.setTelpSupplier(item.getTelpSupplier());
			dtos.add(dto);
		}
		return dtos;
	}

	@Override
	public MstSupplierDto findOne(String kodeSupplier) {
		// TODO Auto-generated method stub
		MstSupplierPk pk = new MstSupplierPk();
		pk.setKodeSupplier(kodeSupplier);
		MstSupplier item = dao.findOne(pk);
		if (item != null) {
			MstSupplierDto dto = new MstSupplierDto();
			dto.setAlamatSupplier(item.getAlamatSupplier());
			dto.setEmailSupplier(item.getEmailSupplier());
			dto.setKodeKota(item.getKodeKota());
			dto.setKodeSupplier(item.getKodeSupplier());
			dto.setNamaSupplier(item.getNamaSupplier());
			dto.setTelpSupplier(item.getTelpSupplier());
			return dto;
		}
		return null;
	}

	@Override
	public void save(MstSupplierDto dto) {
		// TODO Auto-generated method stub
		MstSupplier sup = new MstSupplier();
		sup.setAlamatSupplier(dto.getAlamatSupplier());
		sup.setEmailSupplier(dto.getEmailSupplier());
		sup.setKodeKota(dto.getKodeKota());
		sup.setKodeSupplier(dto.getKodeSupplier());
		sup.setNamaSupplier(dto.getNamaSupplier());
		sup.setTelpSupplier(dto.getTelpSupplier());
		dao.save(sup);
	}

	@Override
	public void update(MstSupplierDto dto) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(String kodeSupplier) {
		// TODO Auto-generated method stub
		MstSupplierPk pk = new MstSupplierPk();
		pk.setKodeSupplier(kodeSupplier);
		dao.delete(pk);
	}

	@Override
	public Map<String, Object> pencarian(String cari, int page) {
		// TODO Auto-generated method stub
		Map<String, Object> map = new HashMap<>();
		int perPage = 5;
		Pageable paging = new PageRequest(page - 1, perPage, new Sort(
				new Sort.Order(Direction.fromString("desc"), "namaSupplier")));
		List<MstSupplier> listSupplier = dao.pencarian(cari, paging);
		List<MstSupplierDto> listDtoSupplier = new ArrayList<>();
		for (MstSupplier item : listSupplier) {
			MstSupplierDto dto = new MstSupplierDto();
			dto.setAlamatSupplier(item.getAlamatSupplier());
			dto.setEmailSupplier(item.getEmailSupplier());
			dto.setKodeKota(item.getKodeKota());
			dto.setKodeSupplier(item.getKodeSupplier());
			dto.setNamaSupplier(item.getNamaSupplier());
			dto.setTelpSupplier(item.getTelpSupplier());
			listDtoSupplier.add(dto);
		}

		int jumlahData = dao.jumlahData(cari);
		int jumlahHalaman = 0;
		jumlahHalaman = jumlahData / perPage;
		if (jumlahData % perPage > 0) {
			jumlahHalaman++;
		}
		map.put("list", listDtoSupplier);
		map.put("jumlah", jumlahHalaman);
		return map;
	}



}
